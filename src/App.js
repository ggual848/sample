
import React, { Component } from 'react';
import DeploymentForm from './DeploymentForm';
import AllDeployments from './AllDeployments';


class App extends Component {
  render() {
    return (
    <div className="App">
        <DeploymentForm />
        <AllDeployments />
    </div>
    );
    }
  }
export default App;

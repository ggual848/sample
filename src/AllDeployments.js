import React, { Component } from 'react';

import { connect } from 'react-redux';

import Deployment from './Deployment';

import EditDeployment from './EditDeployment';

class AllDeployments extends Component {
    render() {
        return (
            <div>
                <h1>All Deployments</h1>
                {this.props.posts.map((post) => (
                    <div key={post.id}>
                        {post.editing ? <EditDeployment post={post} key={post.id} /> :
                            <Deployment key={post.id} post={post} />}
                    </div>
                ))}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}
export default connect(mapStateToProps)(AllDeployments);
